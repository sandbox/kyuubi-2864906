<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\contact_poster\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class MappingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_poster_mappings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $form_id = $path_args[5];
    $config = $this->config('contact_poster.mappings');
    $entity_type_id = 'contact_message';
    $bundle = $form_id;
    foreach (\Drupal::entityManager()->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $bundleFields[$entity_type_id][$field_name]['type'] = $field_definition->getType();
        $bundleFields[$entity_type_id][$field_name]['label'] = $field_definition->getLabel();
      }
    }

    foreach($bundleFields['contact_message'] as $contact_field => $settings){
      $form[$contact_field] = array(
        '#type' => 'textfield',
        '#title' => t('Mapping for '. $contact_field),
        '#required' => TRUE,
        '#default_value' => $config->get('contact_poster.mappings.' . $form_id . '.' . $contact_field),
      );
    }
     return parent::buildForm($form, $form_state);
  }

  /**
  * {@inheritdoc}
  */
 protected function getEditableConfigNames() {
   return [
     'contact_poster.mappings',
   ];
 }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $form_id = $path_args[5];
    // Retrieve the configuration
    $config = $this->config('contact_poster.mappings');
    // Set the submitted configuration settings
    $submitted_values = $form_state->cleanValues()->getValues();
    foreach($submitted_values as $key=>$value){
      $config->set('contact_poster.mappings.' . $form_id . '.' . $key, $form_state->getValue($key));
    }
    $config->save();

parent::submitForm($form, $form_state);
  /*  $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $form_id = $path_args[5];
    $contact_form = \Drupal\contact\Entity\ContactForm::load($form_id);
    $submitted_values = $form_state->cleanValues()->getValues();
    foreach($submitted_values as $key=>$value){
      $contact_form->setThirdPartySetting('contact_poster',$key,$value);
    }*/
  }
}
?>
